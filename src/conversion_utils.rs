/// converts a Rust UTF-8 string to a null-terminated utf-16 vector
pub fn str_to_utf16(string: &str) -> Vec<u16> {
    let mut ret: Vec<u16> = string.encode_utf16().collect();
    ret.push(0);
    ret
}