use std::convert::TryInto;
use std::io;
use std::io::Error;
use std::cell::Cell;
use crate::conversion_utils::str_to_utf16;
use crate::inputs::MouseInput;
use crate::inputs::{KeyCode, Input, KeyboardInput, InputType, InputUnion, KEYEVENTF_KEYUP, SendInput, MouseFlags, ScreenLayout};

#[link(name="User32")]
extern "system" {
    //find a window based on class/window name
    fn FindWindowW(lpClassName: *mut u16, lpWindowName: *mut u16) -> usize;
    // make sure your window is in the foreground
    fn GetForegroundWindow() -> usize;
    fn GetWindowRect(hWnd: usize, lpRect: *mut WindowRect) -> bool;
    fn SetForegroundWindow(hWnd: usize) -> bool;
    fn GetSystemMetrics(nIndex: i32) -> i32;
}

#[repr(C)]
#[derive(Debug)]
pub struct WindowRect {
    left: i32,
    top: i32,
    right: i32,
    bottom: i32
}

impl WindowRect {
    fn new() -> WindowRect {
        WindowRect {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
        }
    }
}

pub struct Window {
    /// Handle to opened window
    pub hwnd: usize,

    /// Seed for RNG
    pub seed: Cell<u64>,

    /// Window dimensions for mouse click
    pub window_rect: WindowRect,

    screen_dimensions: (i32, i32)
}

impl Window{
    /// Find a window with 'title' and return a new 'Window" object
    pub fn attach(title: &str) -> io::Result<Window> {
        // converts 'title' to utf-16
        let mut title = str_to_utf16(title);
        // finds window with 'title'
        let ret = unsafe {
            FindWindowW(std::ptr::null_mut(), title.as_mut_ptr())
        };

        if ret != 0 {
            // successfully got a handle to the window 
            return Ok(Window {
                hwnd: ret,
                seed: Cell::new(unsafe { core::arch::x86_64::_rdtsc() }),
                window_rect: WindowRect::new(),
                screen_dimensions: (unsafe {GetSystemMetrics(ScreenLayout::Width as i32)}, unsafe {GetSystemMetrics(ScreenLayout::Height as i32)})
            })
        }
        else {
            // FindWindow() failed, return out corresponding error
            Err(Error::last_os_error())
        }
    }
    pub fn set_foreground_window(&self) -> io::Result<()> {
        let res = unsafe { SetForegroundWindow(self.hwnd) };
        if res != false {
            return Ok(())
        } 
        else {
            return Err(Error::last_os_error())
        }
    }
    fn normalize_coordinates(&self, x: i32, y: i32) -> (i32, i32) {
        let width = self.screen_dimensions.0;
        let height = self.screen_dimensions.1;
        let new_x = x * 65535 / width;
        let new_y = y * 65535 / height;
        (new_x, new_y)
    }
    pub fn get_window_size(&mut self) -> io::Result<()> {
        let res = unsafe {
            let mut rect = WindowRect::new();
            let res = GetWindowRect(
                self.hwnd, &mut rect);
            self.window_rect = rect;
            res
        };
        if res != false {
            return Ok(())
        }
        else {
            return Err(Error::last_os_error())
        }

    }
    pub fn generate_rand_coords(&self) -> (i32, i32) {
        let left_bound = self.window_rect.left;
        let right_bound = self.window_rect.right;
        let top_bound = self.window_rect.top;
        let bot_bound = self.window_rect.bottom;
        // x
        let x = left_bound + (self.rand() as u32 % (right_bound - left_bound) as u32) as i32;
        let y = top_bound + (self.rand() as u32 % (bot_bound - top_bound) as u32) as i32;
        self.normalize_coordinates(x, y)
    }
    /// Get a random 64-bit number using xorshift
    pub fn rand(&self) -> usize {
        let mut seed = self.seed.get();
        seed ^= seed << 13;
        seed ^= seed >> 17;
        seed ^= seed << 43;
        self.seed.set(seed);
        seed as usize
    }
    pub fn foreground_check(&self) -> bool {
        if unsafe { GetForegroundWindow() } == self.hwnd {
            true
        }
        else {
            false
        }
    }
    /// Formats and sends a stream of 'MouseInput' to 'SendInput()'
    fn mousestream(&self, inputs: &[MouseInput]) -> io::Result<()> {
        // generate an array of mouse inputs to pass to 'SendInput()'
        let mut win_inputs = Vec::new();
        // create inputs based on each mouse input
        for &input in inputs.iter() {
            win_inputs.push(Input {
                typ: InputType::Mouse,
                union: InputUnion {
                    mouse: input
                }
            });
        }
        let res = unsafe {
            SendInput(
                win_inputs.len().try_into().unwrap(),
                win_inputs.as_mut_ptr(),
                std::mem::size_of::<Input>().try_into().unwrap())
        };
        if res as usize != inputs.len() {
            return Err(Error::last_os_error())
        }
        else {
            Ok(())
        }
    }
    pub fn right_click(&self, x: i32, y: i32) -> io::Result<()> {
        self.mousestream(
            &[
                MouseInput {
                    dx: 0,
                    dy: 0,
                    mouse_data: 0,
                    flags: MouseFlags::Movement as u32,
                    time: 0,
                    extra_info: 0
                },
                MouseInput {
                    dx: x,
                    dy: y,
                    mouse_data: 0,
                    flags: MouseFlags::Movement as u32,
                    time: 0,
                    extra_info: 0
                },
                MouseInput {
                    dx: 0,
                    dy: 0,
                    mouse_data: 0,
                    flags: MouseFlags::RightDown as u32,
                    time: 0,
                    extra_info: 0
                },
                MouseInput {
                    dx: 0,
                    dy: 0,
                    mouse_data: 0,
                    flags: MouseFlags::RightUp as u32,
                    time: 0,
                    extra_info: 0
                }
                
            ]
        )
    }
    
    pub fn left_click(&self, x: i32, y: i32) -> io::Result<()> {
        self.mousestream(
            &[
                MouseInput {
                    dx: x,
                    dy: y,
                    mouse_data: 0,
                    flags: MouseFlags::LeftDown as u32,
                    time: 0,
                    extra_info: 0
                },
                MouseInput {
                    dx: x,
                    dy: y,
                    mouse_data: 0,
                    flags: MouseFlags::LeftUp as u32,
                    time: 0,
                    extra_info: 0
                }
            ]
        )
    }
    fn keystream(&self, inputs: &[KeyboardInput]) -> io::Result<()> {
        //generates an array to pass directly to 'SendInput()'
        let mut win_inputs = Vec::new();
        // Create inputs based on each keyboard input
        for &input in inputs.iter() {
            win_inputs.push(Input {
                typ: InputType::Keyboard,
                union: InputUnion {
                    keyboard: input
                }
            });
        }
        let res = unsafe {
            SendInput(
            win_inputs.len().try_into().unwrap(),
            win_inputs.as_mut_ptr(),
            std::mem::size_of::<Input>().try_into().unwrap())
        };
        if res as usize != inputs.len() {
            return Err(Error::last_os_error())
        }
        else {
            Ok(())
        }

    }
    pub fn press(&self, key: u16) -> io::Result<()> {
        self.keystream(&[
            KeyboardInput {
                vk: key,
                scan_code: 0,
                flags: 0,
                time: 0,
                extra_info: 0,
            },
            KeyboardInput {
                vk: key,
                scan_code: 0,
                flags: KEYEVENTF_KEYUP,
                time: 0,
                extra_info: 0,
            }
        ])
    }
    pub fn alt_press(&self, key: u16) -> io::Result<()> {
        if key == KeyCode::Tab as u16 || key == b' ' as u16 {
            return Ok(());
        }
        self.keystream(&[
            KeyboardInput {
                vk: KeyCode::Alt as u16,
                scan_code: 0,
                flags: 0,
                time: 0,
                extra_info: 0,
            },
            KeyboardInput {
                vk: key,
                scan_code: 0,
                flags: 0,
                time: 0,
                extra_info: 0,
            },
            KeyboardInput {
                vk: key,
                scan_code: 0,
                flags: KEYEVENTF_KEYUP,
                time: 0,
                extra_info: 0,
            },
            KeyboardInput {
                vk: KeyCode::Alt as u16,
                scan_code: 0,
                flags: KEYEVENTF_KEYUP,
                time: 0,
                extra_info: 0,
            },
        ])
    }
    pub fn ctrl_press(&self, key: u16) -> io::Result<()> {
        self.keystream(&[
            KeyboardInput {
                vk: KeyCode::Control as u16,
                scan_code: 0,
                flags: 0,
                time: 0,
                extra_info: 0,
            },
            KeyboardInput {
                vk: key,
                scan_code: 0,
                flags: 0,
                time: 0,
                extra_info: 0,
            },
            KeyboardInput {
                vk: key,
                scan_code: 0,
                flags: KEYEVENTF_KEYUP,
                time: 0,
                extra_info: 0,
            },
            KeyboardInput {
                vk: KeyCode::Control as u16,
                scan_code: 0,
                flags: KEYEVENTF_KEYUP,
                time: 0,
                extra_info: 0,
            },
        ])
    }
}

