#[link(name="User32")]
extern "system" {
    pub fn SendInput(cInputs: u32, pInputs: *mut Input, cbSize: i32) -> u32;
    pub fn PostMessageW(hWnd: usize, msg: u32, wParam: usize, lParam: usize) -> usize;
}

#[repr(i32)]
pub enum ScreenLayout {
    Width  = 16,
    Height = 17,
}

#[repr(u32)]
pub enum MsgType {
    LeftButtonDown = 0x0201,
    LeftButtonUp   = 0x0202,
    KeyDown        = 0x0100,
    KeyUp          = 0x0101

}

#[repr(u32)]
pub enum MouseFlags {
    Movement   = 0x8001,
    LeftDown   = 0x0002,
    LeftUp     = 0x0004,
    RightDown  = 0x8008,
    RightUp    = 0x8010,
    MiddleDown = 0x0020,
    MiddleUp   = 0x0040,
}

#[repr(u8)]
pub enum KeyCode {
    Back    = 0x08,
    Tab     = 0x09,
    Return  = 0x0d,
    Shift   = 0x10,
    Control = 0x11,
    Alt     = 0x12,
    Left    = 0x25,
    Up      = 0x26,
    Right   = 0x27,
    Down    = 0x28,
    
}

pub const KEYEVENTF_KEYUP: u32 = 0x002;

/// Different types of inputs for the 'typ' field on 'Input'
#[repr(C)]
#[derive(Clone, Copy)]
pub enum InputType {
    Mouse    = 0,
    Keyboard = 1,
    Hardware = 2,
}

#[repr(C)]
#[derive(Clone, Copy)]
pub struct Input {
    pub typ: InputType,
    pub union: InputUnion,
}

#[repr(C)]
#[derive(Clone, Copy)]
pub union InputUnion {
    pub mouse:    MouseInput,
    pub keyboard: KeyboardInput,
    pub hardware: HardwareInput
}

#[repr(C)]
#[derive(Clone, Copy)]
pub struct KeyboardInput {
    pub vk:         u16,
    pub scan_code:  u16,
    pub flags:      u32,
    pub time:       u32,
    pub extra_info: usize,
}

#[repr(C)]
#[derive(Clone, Copy)]
pub struct MouseInput {
    pub dx:         i32,
    pub dy:         i32,
    pub mouse_data: u32,
    pub flags:      u32,
    pub time:       u32,
    pub extra_info: usize,
}

#[repr(C)]
#[derive(Clone, Copy)]
pub struct HardwareInput {
    msg:    u32,
    lparam: u16,
    hparam: u16,

}